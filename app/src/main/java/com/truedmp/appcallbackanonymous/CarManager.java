package com.truedmp.appcallbackanonymous;

public class CarManager {

    public void getCitroen(CitroenInterface citroenInterface) {
        Citroen citroen = new Citroen(100, "Blue", "Limusine");
        citroenInterface.getCitroen(citroen);
    }

    public void getFiat(FiatInterface fiatInterface) {
        Fiat fiat = new Fiat(90, "Black", "Hatchback");
        fiatInterface.getFiat(fiat);
    }

    public void getOpel(OpelInterface opelInterface) {
        Opel opel = new Opel(80, "White", "Hatchback");
        opelInterface.getOpel(opel);
    }

}
