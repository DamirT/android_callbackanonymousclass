package com.truedmp.appcallbackanonymous;

public abstract class Car {

    private int horsePower;
    private String color;
    private String type;

    public Car(int horsePower, String color, String type) {
        this.horsePower = horsePower;
        this.color = color;
        this.type = type;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setType(String type) {
        this.type = type;
    }
}


