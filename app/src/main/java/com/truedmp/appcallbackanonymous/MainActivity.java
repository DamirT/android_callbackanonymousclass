package com.truedmp.appcallbackanonymous;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{

    TextView carHs;
    TextView carColor;
    TextView carType;

    Button citroenButton;
    Button fiatButton;
    Button opelButton;

    CarManager carManager = new CarManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carHs = findViewById(R.id.car_hs);
        carColor = findViewById(R.id.car_color);
        carType = findViewById(R.id.car_type);

        citroenButton = findViewById(R.id.citroen_button);
        fiatButton = findViewById(R.id.fiat_button);
        opelButton = findViewById(R.id.opel_button);

        citroenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carManager.getCitroen(new CitroenInterface() {
                    @Override
                    public void getCitroen(Citroen citroen) {
                        carHs.setText("" + citroen.getHorsePower());
                        carColor.setText(citroen.getColor());
                        carType.setText(citroen.getType());
                    }
                });
            }
        });
        fiatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carManager.getFiat(new FiatInterface() {
                    @Override
                    public void getFiat(Fiat fiat) {
                        carHs.setText("" + fiat.getHorsePower());
                        carColor.setText(fiat.getColor());
                        carType.setText(fiat.getType());
                    }
                });
            }
        });

        //this is same example as two of them above but with lambda expression
        opelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carManager.getOpel(opel -> {
                    carHs.setText("" + opel.getHorsePower());
                    carColor.setText(opel.getColor());
                    carType.setText(opel.getType());
                });
            }
        });
    }

}